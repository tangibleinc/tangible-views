<?php

/**
 * No Replies Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'No replies found.', 'bbpress' ); ?></p>
</div>
