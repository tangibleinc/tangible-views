<?php

/**
 * No Search Results Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'No search results found.', 'bbpress' ); ?></p>
</div>
