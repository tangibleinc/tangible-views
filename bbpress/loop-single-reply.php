<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">

	<div class="bbp-meta">
<?php /*
		<span class="bbp-reply-post-date"><?php bbp_reply_post_date(0, true); ?></span>
*/ ?>
		<?php if ( bbp_is_single_user_replies() ) : ?>

			<span class="bbp-header">
				<?php _e( 'in reply to: ', 'bbpress' ); ?>
				<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
			</span>

		<?php endif; ?>

		<a href="<?php bbp_reply_url(); ?>" class="bbp-reply-permalink">#<?php bbp_reply_id(); ?></a>

		<?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

		<?php
    if (bbp_is_topic(bbp_get_reply_id())) bbp_topic_admin_links([
      'links' => [
        // @see bbpress/includes/topics/template.php bbp_get_topic_admin_links
        'edit'  => bbp_get_topic_edit_link (),
        //'close' => bbp_get_topic_close_link(),
        //'stick' => bbp_get_topic_stick_link(),
        //'merge' => bbp_get_topic_merge_link(),
        'trash' => bbp_get_topic_trash_link(),
        //'spam'  => bbp_get_topic_spam_link (),
        'reply' => bbp_get_topic_reply_link()
      ],
    ]); else bbp_reply_admin_links([
      'links' => [
        // @see bbpress/includes/replies/template.php bbp_get_reply_admin_links
				'edit'  => bbp_get_reply_edit_link (),
				//'move'  => bbp_get_reply_move_link (),
				//'split' => bbp_get_topic_split_link(),
				'trash' => bbp_get_reply_trash_link(),
				//'spam'  => bbp_get_reply_spam_link (),
				'reply' => bbp_thread_replies() ? bbp_get_reply_to_link() : false
      ],
    ]); ?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

	</div><!-- .bbp-meta -->

</div><!-- #post-<?php bbp_reply_id(); ?> -->

<div <?php bbp_reply_class(); ?>>

	<div class="bbp-reply-author">

		<?php do_action( 'bbp_theme_before_reply_author_details' ); ?>

		<div class="bbp-reply-post-date"><?php bbp_reply_post_date(0, true); ?></div>

		<?php bbp_reply_author_link([
      'size' => 60,
      //'sep' => '<br />', 'show_role' => true
    ]); ?>

		<?php /*if ( bbp_is_user_keymaster() ) : ?>

			<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

			<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

			<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

		<?php endif; */?>

		<?php do_action( 'bbp_theme_after_reply_author_details' ); ?>

	</div><!-- .bbp-reply-author -->

	<div class="bbp-reply-content">

		<?php do_action( 'bbp_theme_before_reply_content' ); ?>

		<?php bbp_reply_content(); ?>

		<?php do_action( 'bbp_theme_after_reply_content' ); ?>

	</div><!-- .bbp-reply-content -->

</div><!-- .reply -->
