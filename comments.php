<?php if ( have_comments() ) {
?>
  <h3 id="comments">
    <?php
      if ( 1 == get_comments_number() ) {
        /* translators: %s: post title */
        printf( __( 'One response to %s' ),  '&#8220;' . get_the_title() . '&#8221;' );
      } else {
        /* translators: 1: number of comments, 2: post title */
        printf( _n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number() ),
          number_format_i18n( get_comments_number() ),  '&#8220;' . get_the_title() . '&#8221;' );
      }
    ?>
  </h3>

  <div class="navigation">
    <div class="alignleft"><?php previous_comments_link() ?></div>
    <div class="alignright"><?php next_comments_link() ?></div>
  </div>

  <ul class="commentlist list-unstyled">
  <?php wp_list_comments([
    'type' => 'comment',
    'style' => 'ul',
    'callback' => 'Tangible\\Views\\comment_list',
  ]); ?>
  </ul>

  <div class="navigation">
    <div class="alignleft"><?php previous_comments_link() ?></div>
    <div class="alignright"><?php next_comments_link() ?></div>
  </div>
<?php } else {

  // No comments
}

if ( comments_open() ) {
  comment_form();
} else {

}
