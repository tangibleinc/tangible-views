<?php

use Tangible\Views as views;

require_once __DIR__ . '/vendor/tangible/plugin-framework/index.php';
require_once __DIR__ . '/vendor/tangible/plugin-updater/index.php';

define('TANGIBLE_VIEWS_VERSION', '1.0.0');

function tangible_views() {

  static $theme;
  if ( $theme ) return $theme;

  $framework = tangible();

  $theme = $framework->register_theme([
    'name' => 'tangible-views',
    'title' => 'Tangible Views',
    'version' => TANGIBLE_VIEWS_VERSION,
    'file_path' => __FILE__,
    // 'item_id' => 13665,
  ]);

  tangible_plugin_updater()->register_theme([
    'name' => $theme->name,
    'file' => __FILE__,
    // 'license' => ''
  ]);

  // Template

  add_filter('template_include', function($template) {
    // Always load index to render templates
    return __DIR__ . '/index.php';
  }, 1000);

  if (!function_exists('tangible_template')) return;

  $html = tangible_template();

  // Features

  require_once __DIR__.'/includes/index.php';
}

tangible_views();
