<?php

namespace Tangible\Views;

if ( ! function_exists('bbpress') ) return;

// Custom stylesheet

add_action('wp_print_styles', function() {
  wp_deregister_style( 'bbp-default' );
}, 15);

add_action('wp_head', function() {
  ?><link rel="stylesheet" href="<?= get_template_directory_uri() ?>/assets/build/bbpress.min.css"><?php
});

// Switch avatar and user name
function switch_avatar_and_name($author_links) {
  $avatar = $author_links[0];
  $author_links[0] = $author_links[1];
  $author_links[1] = $avatar;
  return $author_links;
}

add_filter( 'bbp_get_author_links', 'Tangible\\Views\\switch_avatar_and_name', 10, 1);
add_filter( 'bbp_get_topic_author_links', 'Tangible\\Views\\switch_avatar_and_name', 10, 1);
add_filter( 'bbp_get_reply_author_links', 'Tangible\\Views\\switch_avatar_and_name', 10, 1);

// Make bbPress work on localhost

add_filter( 'bbp_verify_nonce_request_url', function( $requested_url ) {
  if ($_SERVER['SERVER_NAME']==='localhost' || substr($_SERVER['SERVER_NAME'], 0, 6)==='.local')
    return 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  return $requested_url;
}, 999, 1 );
