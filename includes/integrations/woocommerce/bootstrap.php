<?php

// Add Bootstrap classes to fields

add_filter( 'woocommerce_form_field_args', function( $args, $key, $value = null ) {
  // Start field type switch case.
  switch ( $args['type'] ) {
    /* Targets all select input type elements, except the country and state select input types */
    case 'select' :
      // Add a class to the field's html element wrapper - woocommerce
      // input types (fields) are often wrapped within a <p></p> tag.
      $args['class'][] = 'form-group';
      // Add a class to the form input itself.
      $args['input_class']       = array( 'form-control', 'input-lg' );
      $args['label_class']       = array( 'control-label' );
      $args['custom_attributes'] = array(
        'data-plugin'      => 'select2',
        'data-allow-clear' => 'true',
        'aria-hidden'      => 'true',
        // Add custom data attributes to the form input itself.
      );
      break;
    // By default WooCommerce will populate a select with the country names - $args
    // defined for this specific input type targets only the country select element.
    case 'country' :
      $args['class'][]     = 'form-group single-country';
      $args['label_class'] = array( 'control-label' );
      break;
    // By default WooCommerce will populate a select with state names - $args defined
    // for this specific input type targets only the country select element.
    case 'state' :
      // Add class to the field's html element wrapper.
      $args['class'][] = 'form-group';
      // add class to the form input itself.
      $args['input_class']       = array( '', 'input-lg' );
      $args['label_class']       = array( 'control-label' );
      $args['custom_attributes'] = array(
        'data-plugin'      => 'select2',
        'data-allow-clear' => 'true',
        'aria-hidden'      => 'true',
      );
      break;
    case 'password' :
    case 'text' :
    case 'email' :
    case 'tel' :
    case 'number' :
      $args['class'][]     = 'form-group';
      $args['input_class'] = array( 'form-control', 'input-lg' );
      $args['label_class'] = array( 'control-label' );
      break;
    case 'textarea' :
      $args['input_class'] = array( 'form-control', 'input-lg' );
      $args['label_class'] = array( 'control-label' );
      break;
    case 'checkbox' :
      $args['label_class'] = array( 'custom-control custom-checkbox' );
      $args['input_class'] = array( 'custom-control-input', 'input-lg' );
      break;
    case 'radio' :
      $args['label_class'] = array( 'custom-control custom-radio' );
      $args['input_class'] = array( 'custom-control-input', 'input-lg' );
      break;
    default :
      $args['class'][]     = 'form-group';
      $args['input_class'] = array( 'form-control', 'input-lg' );
      $args['label_class'] = array( 'control-label' );
      break;
  } // end switch ($args).
  return $args;

}, 10, 3 );

// Add to cart

add_filter( 'woocommerce_loop_add_to_cart_args', function( $args, $product ) {
  $args['class'] = str_replace('button','btn btn-outline-primary', 'button');
  return $args;
}, 10, 2 );
