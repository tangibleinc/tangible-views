<?php

if ( ! function_exists('WC') ) return;

require_once __DIR__.'/setup.php';
require_once __DIR__.'/bootstrap.php';
