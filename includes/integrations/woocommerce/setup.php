<?php

// Remove notices

\WC_Admin_Notices::remove_notice( 'no_secure_connection' );
\WC_Admin_Notices::remove_notice( 'template_files' );
\WC_Admin_Notices::remove_notice( 'wootenberg' );

// https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-slider' );

// Don't show page title in templates

add_filter('woocommerce_show_page_title', '__return_false');

// Skip post code validation

add_filter('woocommerce_default_address_fields', function( $address_fields ) {
  $address_fields['postcode']['validate'] = false;
  return $address_fields;
});

add_filter('woocommerce_checkout_fields', function( $fields ) {
  unset($fields['billing']['billing_postcode']['validate']);
  unset($fields['shipping']['shipping_postcode']['validate']);
  return $fields;
}, 99);
