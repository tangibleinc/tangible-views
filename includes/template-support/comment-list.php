<?php

namespace Tangible\Views;

// For comments list - see ../comments.php

function comment_list($comment, $args, $depth) {
  if ( 'div' === $args['style'] ) {
    $tag       = 'div';
    $add_below = 'comment';
  } else {
    $tag       = 'li';
    $add_below = 'div-comment';
  }

  $classes = ['list-unstyled'];
  if (!empty($args['has_children'])) $classes[] = 'parent';

  ?><<?php echo $tag; ?> <?php comment_class($classes); ?> id="comment-<?php comment_ID() ?>"><?php

  if ('div' != $args['style']) { ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
  }

  ?>
  <div class="comment-author vcard"
    style="font-size:.9rem;text-align:right;background-color:#fbfbfb;"
  ><?php
    echo human_time_diff(mysql2date('U', $comment->comment_date)).' ago ';
    //if ( $args['avatar_size'] != 0 ) {
    echo get_avatar( $comment, 20 /*$args['avatar_size']*/ );
    //}
    ?>&nbsp;<span class="comment-author-link"><?= get_comment_author_link($comment->comment_ID) ?></span>
  </div><?php

  if ( $comment->comment_approved == '0' ) { ?>
      <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php
  }
  /*
  ?>
  <div class="comment-meta commentmetadata">
    <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
        printf(
            __('%1$s at %2$s'),
            get_comment_date(),
            get_comment_time()
        ); ?>
    </a><?php
    edit_comment_link('Edit', '  ', '' ); ?>
  </div>

  <?php */ comment_text(); ?>

  <div class="reply" style="font-size:.9rem;text-align:right;border-bottom:1px solid #ddd;padding-bottom:3px;margin-bottom:3px"><?php
    comment_reply_link(
        array_merge(
            $args,
            array(
                'add_below' => $add_below,
                'depth'     => $depth,
                'max_depth' => $args['max_depth']
            )
        )
    ); ?>
  </div><?php

  if ('div' != $args['style']) {
    ?></div><?php
  }

  // Leave open for children
}
