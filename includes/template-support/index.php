<?php

require_once __DIR__.'/comment-list.php';
require_once __DIR__.'/comment-form.php';
require_once __DIR__.'/nav-walker.php';
require_once __DIR__.'/pagination.php';

// Theme supports

add_action('after_setup_theme', function() {

  // Use HTML5 markup
  add_theme_support('html5', [
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
  ]);

  // Featured images
  add_theme_support('post-thumbnails');

  // Register default menu
  register_nav_menus(['main_menu' => 'Main Menu']);
});

// Comment support
add_action('wp_enqueue_scripts', function() {
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script('comment-reply');
  }
});

/**
 * Disable automatic redirect of unknown routes
 *
 * @see wp-includes/default-filters.php
 */

add_filter( 'redirect_canonical', '__return_false' );

remove_action('template_redirect', 'redirect_canonical');
remove_filter('template_redirect', 'redirect_canonical');

remove_action('template_redirect', 'maybe_redirect_404');
remove_action('template_redirect', 'wp_old_slug_redirect');
remove_action('template_redirect', 'wp_redirect_admin_locations', 1000);
