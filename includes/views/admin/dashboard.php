<?php

namespace Tangible\Views;

use Tangible\Views as views;

// Dashboard

add_action('wp_dashboard_setup', function() use ($html) {

  $views_path = views\get_template_folder_path();

  foreach ([
    'left', 'right'
  ] as $key) {

    $files = glob( "$views_path/admin/dashboard/$key/*.html" );

    $main = $key==='left';
    $metabox_fn = 'Tangible\\Views\\render_metabox';

    foreach ($files as $file) {

      ob_start();
      include($file);
      $content = ob_get_clean();

      $id = 'tangible_views_dashboard_widget_' . uniqid();

      $title = str_replace('.html', '', basename($file));

      // Remove number plus dot ("X.") used to define metabox order in file names
      if (strpos($title, '.')!==false) {
        $title = trim(implode('.', array_slice(explode('.', $title), 1)));
      }

      $metabox_data = [
        'id' => $id,
        'title'=>$title,
        'file' => $file,
        'content' => $html->render( $content )
      ];

      if ($main) {

        // Main/left

        wp_add_dashboard_widget(
          $id,
          $title,
          $metabox_fn,
          '',
          $metabox_data
        );

      } else {

        // Side/right

        add_meta_box(
          $id,
          $title,
          $metabox_fn,
          'dashboard',
          'side',
          'high',
          $metabox_data
        );
      }
    }
  }
});

function render_metabox( $post, $data ) {

  $content = isset($data['args']) && isset($data['args']['content'])
    ? $data['args']['content']
    : ''
  ;

  echo $content;
}
