<?php
/**
 * Views: Admin - local $html, $views_path
 */

// Content structure

$content_html_path = "$views_path/admin/content/index.html";

if ( file_exists($content_html_path) ) {
  $html->render_content_tags(
    $html->load_template_with_context( $content_html_path )
  );
}

if ( is_admin() && !(wp_doing_ajax() || wp_is_json_request()) ) {

  // Dashboard
  require_once __DIR__.'/dashboard.php';

  // Styles

  $file_path = "$views_path/admin/styles/index.scss";

  if ( file_exists($file_path) ) {
    $html->enqueue_sass_file( $file_path );
  }
}
