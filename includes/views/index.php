<?php

namespace Tangible\Views;

use Tangible\Views as views;

// Views related functions

function get_template_folder_path() {
  static $path;
  if ($path) return $path;
  return $path = is_child_theme()
    ? get_stylesheet_directory() // Parent: get_template_directory()
    : WP_CONTENT_DIR.'/views' // No child theme
  ;
}

function get_template_folder_url() {
  static $url;
  if ($url) return $url;
  return $url = is_child_theme()
    ? get_stylesheet_directory_uri() // Parent: get_template_directory_uri()
    : WP_CONTENT_URL.'/views' // No child theme
  ;
}

function get_views_theme_path() {
  return get_template_directory();
}

// Define views URL and path as variable types

$html->set_variable_type('url', 'views', get_template_folder_url());
$html->set_variable_type('path', 'views', get_template_folder_path());

// Views: Functions

$views_path = views\get_template_folder_path();

$views_functions_path = "$views_path/functions.php";

if ( file_exists($views_functions_path) ) {
  include_once $views_functions_path;
}

require_once __DIR__.'/admin/index.php';
require_once __DIR__.'/router.php';
require_once __DIR__.'/settings.php';
