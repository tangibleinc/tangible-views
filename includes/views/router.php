<?php

namespace Tangible\Views;

use Tangible\Views as views;

/**
 * Get template data from URL route
 */
function get_route_template_data($full_route, $source = 'file') {

  $html = tangible_template();

  $route_parts = explode('/', $full_route);
  $views_path = views\get_template_folder_path();

  $result = [
    'template' => '',
    'found_index' => false,
    'found_exact_match' => false,
  ];

  if ($source==='file' && !file_exists($views_path)) return $result;

  /**
   * Gather template partials
   *
   * Routes are searched depth-first. For example:
   *
   * - route-1/route-2/*.html
   * - route-1/*.html
   * - *.html
   *
   * TODO: Support loading from template post type
   */

  $template_parts = [
    'root' => '',
    'common' => [],
    'header' => '',
    'index' => '',
    'footer' => '',
  ];

  do {

    $check_route = implode('/', $route_parts);
    if (empty($check_route) && $full_route==='/') break;
    if ($check_route==='/') $check_route = '';

    foreach ($template_parts as $key => $value) {

      // Single file per partial type, except for common

      if (!is_array($value) && !empty($value)) continue;

      $file = $views_path . $check_route . '/' . $key . '.html';

      if (!file_exists($file)) continue;

      if ($key==='index') {
        $result['found_index'] = true;
        $result['found_exact_match'] = $check_route === ('/'.$full_route);
      }

      if (is_array($value)) $value []= $file; //$content;
      else $value = $file; //$content;

      $template_parts[$key] = $value;
    }

    array_pop($route_parts);

  } while (!empty($route_parts));

  // tgbl()->see('template parts', $template_parts);

  /**
   * Combine to single template
   */

  // Use prefixed variables since templates are included directly in this scope
  $_parts = $template_parts;
  $_result = $result;

  ob_start();

  foreach ($_parts as $_key => $_value) {
    if (empty($_value)) continue;
    if (!is_array($_value)) $_value = [$_value];
    foreach ($_value as $file) {

      /**
       * Give each template partial a context
       *
       * Below block is the same as $html->load_template_with_context( $file ), but
       * all templates are included here so PHP in templates can run in the same scope.
       */

      ?><PushContext
        views_root_path="<?= $views_path ?>"
        path="<?= dirname($file) ?>"
        file="<?= basename($file) ?>"
      /><?php

      include $file;

      ?><PopContext /><?php
    }
  }

  $template = ob_get_clean();

  // tgbl()->see('template', $template);

  $_result['template'] = $template;

  return $_result;
}
