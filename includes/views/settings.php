<?php

/**
 * Views theme settings
 *
 * Used in root index.php
 */

$html->views_theme_settings = [
  'jquery' => true,
  'bootstrap' => false,
];

$html->add_open_tag('ViewsTheme', function($atts, $nodes) use ($html) {

  // Enable/disable features

  if (isset($atts['enable'])) {
    $features = array_map('trim', explode(',', $atts['enable']));
    foreach ($features as $feature) {
      $html->views_theme_settings[ $feature ] = true;
    }
  }

  if (isset($atts['disable'])) {
    $features = array_map('trim', explode(',', $atts['disable']));
    foreach ($features as $feature) {
      $html->views_theme_settings[ $feature ] = false;
    }
  }

  if (isset($atts['bootstrap'])) {
    $html->views_theme_settings['bootstrap'] = $atts['bootstrap']==='false'
      ? false
      : $atts['bootstrap'] // 4 or 5
    ;
  }

  if (isset($atts['admin-menu']) && $atts['admin-menu']==='false') {

    // Remove admin menu from frontend
    add_filter( 'show_admin_bar', '__return_false' );
    // And its inlined script and styles
    remove_action( 'wp_head', '_admin_bar_bump_cb' );

    add_action( 'wp_enqueue_scripts', function() {
      wp_dequeue_script('admin-bar');
      wp_dequeue_style('admin-bar');
      wp_dequeue_script('hoverintent-js');
    }, 999);

  }

});
