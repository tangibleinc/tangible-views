<?php

namespace Tangible\Views;

use Tangible\Views as views;

if (!function_exists('tangible_template')) {
  ?>Please activate Loops & Logic or Tangible Blocks.<?php
  exit;
}

// Catch missing files

$parts = explode('/', $wp->request);
$part = array_pop($parts);
if (strpos($part, '.')!==false) {
  status_header(404);
  return;
}

// Route templates

$full_route = '/' . $wp->request;

$result = views\get_route_template_data( $full_route );

$template = $result['template'];
$found_index = $result['found_index'];

if ($found_index) {
  // Allow template to manually set status code
  status_header(200);
  $wp_query->is_404 = false;
}

// Render templates early, for styles to be enqueued in wp_head

$html = tangible_template();

// Set default meta tags

$html->schedule_meta('content_type',
  get_bloginfo('html_type') . '; charset=' . get_bloginfo('charset') . ';'
);
$html->schedule_meta('viewport', 'width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover');
$html->schedule_meta('title', wp_get_document_title());

/**
 * Support Exit tag
 * @see vendor/tangible/template/tags/exit.php
 */
$content =$html->render_with_catch_exit( $template ); // Was $html->render( $template )

// Theme settings

$settings = $html->views_theme_settings;

// Assets

if ($settings['jquery']) {
  wp_enqueue_script('jquery');
} else {
  wp_deregister_script('jquery');
}

// Document

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php

    $html->render_scheduled_meta();

    if ($settings['bootstrap']) {

      // https://getbootstrap.com/docs/5.1/getting-started/introduction/
      // https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js
      // https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css

      ?>
      <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/assets/vendor/<?=
        $settings['bootstrap']==='5'
          ? 'bootstrap-5/bootstrap.min.css?version=5.1.3'
          : 'bootstrap-4/bootstrap.min.css?version=4.6.0'
      ?>">
      <?php
    }

    wp_head();
  ?>
</head>
<body <?php body_class(); ?>>
<?php

if ($found_index) {

  echo $content;

} else {
  if (have_posts()) {
    while (have_posts()) {
      the_post();
      ?><h1><?= the_title() ?></h1><?php
      ?><?= the_content() ?><?php
    }
  }
}

?>
<div id="footer-scripts">
<?php
  if ($settings['bootstrap']) {
    ?>
    <script src="<?= get_template_directory_uri() ?>/assets/vendor/<?=
        $settings['bootstrap']==='5'
          ? 'bootstrap-5/bootstrap.bundle.min.js?version=5.1.3'
          : 'bootstrap-4/bootstrap.bundle.min.js?version=4.6.0'
      ?>"></script>
    <?php
  }

  wp_footer();
?>
</div>
</body>
</html>