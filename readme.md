# Tangible Views

Theme based on Bootstrap and Tangible Loops & Logic for templates.

It loads templates from a child theme, based on the route.

Each page/route can consist of the following "partial" templates.

- `root.html`
- `common.html`
- `header.html`
- `index.html`
- `footer.html`

These are all optional except for `index.html`.

They are loaded in the order of the above list.

Template partials are loaded only once, from the deepest route where it's found - except for `common.html`, which are loaded from all routes where it's found.
