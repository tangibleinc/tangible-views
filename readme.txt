
== Changelog ==

= 1.0.0 =

Release Date: 2022-07-01

- **Breaking change**: Bootstrap is no longer loaded by default.
  Use `<ViewsTheme bootstrap=4 />` or `<ViewsTheme bootstrap=5 />` in `views/common.html`.
- New build pipeline and theme updater

= 0.3.0 =

Release Date: 2022-02-08

- Bundle Bootstrap 5: Optionally loaded with `<ViewsTheme bootstrap=5 />`
- Simplify loading of admin styles and content structure templates

= 0.2.8 =

Release Date: 2021-11-01

- Update to Bootstrap 4.6.0
- Support Exit tag
- Improve when requested file not found

= 0.2.6 =

Release Date: 2021-06-15

- Compatibility with L&L version 1.1.7: Don't enqueue admin views style during AJAX request
- Improve default meta tag for viewport: Add viewport-fit=cover to handle the "notch" in iPhone and Google Pixel phones

= 0.2.5 =

Release Date: 2021-03-10

- Add support for admin views style and functions (`index.scss` and `functions.php`)
- Update dependencies

= 0.2.4 =

Release Date: 2020-11-15

- Add theme settings and ViewsTheme tag to control behavior
- Support not enqueuing default jQuery or Bootstrap, in case templates include their own bundles

= 0.2.3 =

Release Date: 2020-09-08

- Add support for content structure templates and admin styles

= 0.2.2 =

Release Date: 2020-09-02

- Improve support for SEO meta tags
- Add support for dashboard widget templates

= 0.1.8 =

Release Date: 2020-08-13

- Improve control of template redirect
- Bootstrap integration
  - BBPress custom theme
  - WooCommerce custom templates
- Implement theme updater

= 0.1.5 =

Release Date: 2020-08-03

- Improve template loading in shared scope
