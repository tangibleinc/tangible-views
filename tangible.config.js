module.exports = {
  build: [
    {
      src: 'assets/src/bbpress/index.scss',
      dest: 'assets/build/bbpress.min.css',
    },
    // {
    //   src: 'src/index.js',
    //   dest: 'build/app.min.js',
    // },
  ],
  format: [
    '**/*.{php,js,json,scss}',
    '!assets/build',
    '!bbpress'
  ]
}
